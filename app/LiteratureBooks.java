package com.app;

public class LiteratureBooks extends Books {

    public LiteratureBooks(String bookName, String authorName, int bookCount, int price, int bookId) {
        super(bookName, authorName, bookCount, price, bookId);
    }

    LiteratureBooks() {
        super();
    }

    void addToStocks() {
        BookStore.stocks.add(new LiteratureBooks("Thirukural", "Thiruvalluvar", 50, 200, 101));
        BookStore.stocks.add(new LiteratureBooks("Thirumandiram", "Thirumular", 100, 150, 102));
        BookStore.stocks.add(new LiteratureBooks("Purananuru", "kaniyan", 200, 100, 103));
    }
}
