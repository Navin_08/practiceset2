package com.app;


public class Books {
    String bookName;
    String authorName;
    int bookCount;
    int price;
    int bookId;
    public Books(String bookName, String authorName, int bookCount, int price, int bookId){
        this.bookName=bookName;
        this.bookCount=bookCount;
        this.authorName=authorName;
        this.price=price;
        this.bookId=bookId;
    }

    public Books() {
    }
}

