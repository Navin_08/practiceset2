package com.app;

import java.io.Console;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;


public class BookStore {
       static int customerId=123;
       static ArrayList<Customer> customers = new ArrayList<>();

       static ArrayList<Books> stocks = new ArrayList<>();

    public void buyBooks(int bid) {
        Scanner sc = new Scanner(System.in);
        boolean b = false;
        for (int k = 0; k < stocks.size(); k++) {
            int copy = 0;
            if (bid == stocks.get(k).bookId) {
                System.out.println("Enter the Number of copy of book:");
                try {
                    copy = sc.nextInt();
                }catch (InputMismatchException e){
                    System.out.println("Enter the number only");
                }

                if (copy > stocks.get(k).bookCount) {
                    System.err.println("These many copies are not available");
                } else {
                    b = true;
                    stocks.get(k).bookCount = stocks.get(k).bookCount - copy;
                    System.out.println("You bought a book and Id: " + stocks.get(k).bookName + "\nBalance in stock:  " + stocks.get(k).bookCount);
                    loggedIn(true);
                }

            }
        }
        if (!b) {
            System.err.println("Book not bought or found");
            loggedIn(true);
        }
    }

    public void buyBooks(String bookName) {
        Scanner sc = new Scanner(System.in);
        boolean b = false;
        for (int j = 0; j < stocks.size(); j++) {
            if (bookName.equals(stocks.get(j).bookName)) {
                System.out.println("Enter the number of copy of book:");
                int copy = 0;
                try {
                    copy = sc.nextInt();
                }catch (InputMismatchException e){
                    System.out.println("Enter number only");
                    loggedIn(true);
                }

                if (copy > stocks.get(j).bookCount) {
                    System.err.println("These many copies are not available");
                } else {
                    b = true;
                    stocks.get(j).bookCount -= copy;
                    System.out.println("You bought a book and Id " + stocks.get(j).bookName + "\nBalance in stock: " + stocks.get(j).bookCount);
                    displayStocks();
                    loggedIn(true);
                }
            }
        }
        if (!b) {
            System.err.println("Book not bought or found");
            loggedIn(true);
        }
    }

    public static void displayStocks(){
        System.out.println("BookName\t\t\t\tAuthorName\t\t\tBookCount\t\tPrice\t\tBookId");
        for(int i=0;i<stocks.size();i++){
           // System.out.printf("%5s\t\t%5s\t\t%d\t\t%d\t\t%d",stocks.get(i).bookName,stocks.get(i).authorName,stocks.get(i).bookCount,stocks.get(i).price+"\t\t\t\t"+stocks.get(i).bookId);
            System.out.println(stocks.get(i).bookName+"\t\t\t\t\t"+stocks.get(i).authorName+"\t\t\t\t\t\t"+stocks.get(i).bookCount+"\t\t"+stocks.get(i).price+"\t\t"+stocks.get(i).bookId);
        }
    }

    public  void createAccount(){
        Scanner sc = new Scanner(System.in);
        Console console = System.console();

        String name,password;
        System.out.println("Enter your name:");
        name = sc.next();
        System.out.println("Enter your password");
        password = sc.next();
        int id = customerId++;
        System.out.println("Your customer id: "+id);
        customers.add(new Customer(name,id,password));

    }
     void logout() {
         int choice = 0;
         do {
             Scanner sv = new Scanner(System.in);
             System.out.println("1.Create account");
             System.out.println("2.login");
             System.out.println("3.Exit");
             try {
                 choice = sv.nextInt();
                 switch (choice) {
                     case 1:
                         createAccount();
                         break;
                     case 2:
                         login();
                         break;
                     case 3:
                         System.exit(0);
                     default:
                         System.out.println("Enter the valid choice");
                 }
             } catch (InputMismatchException e) {
                 System.out.println("Enter the number only");
                 logout();
             }
         } while (choice != 0);
     }
    public  void loggedIn(boolean b){
        Scanner sc = new Scanner(System.in);
        if(b){
            System.out.println("1.buy books");
            System.out.println("2.exit");
            int choice = 0;
            try {
                choice = sc.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Enter the number only");
            }

            switch (choice){
                case 1:
                    displayStocks();   //fixme: align properly show details
                    System.out.println("Enter the book name or book id to buy:");
                    System.out.println("1.Book Name");
                    System.out.println("2.Book Id");
                    int no=0;

                    try{
                        no = sc.nextInt();
                    }catch (InputMismatchException e){
                        System.out.println("Enter number only");
                        loggedIn(true);
                    }

                    if(no==1){
                        System.out.println("Enter the bookName: ");
                        String bookName= sc.next();
                        buyBooks(bookName);
                    }else if(no==2){
                        int bid;
                        try {
                            System.out.println("Enter the bookId: ");
                            bid = sc.nextInt();
                            buyBooks(bid);
                        }catch (InputMismatchException e){
                            System.out.println("Enter number only");
                            loggedIn(true);
                        }
                    }
                    break;
                case 2:
                    logout();
                    break;
                default:
                    System.out.println("Enter the valid choice");
            }
        }else{
            login();
        }
    }
    public  void login(){
        Scanner sc = new Scanner(System.in);
        String name,pass;
        boolean b= false;
        System.out.println("Enter your name");
        name =sc.next();
        for(int i=0;i<customers.size();i++){
            if(name.equals(customers.get(i).name)){
                b=true;
                System.out.println("Enter your password");
                pass =sc.next();
                if(pass.equals(customers.get(i).password)){
                    b=true;
                    loggedIn(b);
                }
            }
        }
        if(!b){
            System.out.println("Login failed");
        }
    }

    public static void main(String[] args) {

        LiteratureBooks lb = new LiteratureBooks();
        lb.addToStocks();
        ProgrammingBooks pb = new ProgrammingBooks();
        pb.addToStocks();
        displayStocks();
        Scanner sc = new Scanner(System.in);
        int choice = 0;

//        while(true){
//            int choice = 0;
//            System.out.println("1.Create Account");
//            System.out.println("2.login"); //fixme: password hide console
//            System.out.println("3.Exit");
//
//            try {
//                choice = sc.nextInt();
//            }catch (InputMismatchException e){
//                System.out.println("Enter number only"); //Todo: stop the infinite loop when exception catches.
//            }
//                BookStore store = new BookStore();
//                stocksCheck();
//                switch(choice){
//                    case 1:
//                        store.createAccount();
//                        break;
//                    case 2:
//                        store.login();
//                    case 3:
//                        System.exit(0);
//                    default:
//                        System.out.println("Enter the valid choice");
//                }
//
//        }
        do {
            System.out.println("1.Create Account");
            System.out.println("2.login"); //fixme: password hide console
            System.out.println("3.Exit");

            try {
                choice = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter number only"); //Todo: stop the infinite loop when exception catches.--> Completed
            }
            BookStore store = new BookStore();
            stocksCheck();
            switch (choice) {
                case 1:
                    store.createAccount();
                    break;
                case 2:
                    store.login();
                case 3:
                    System.exit(0);
                default:
                    System.out.println("Enter the valid choice");
            }

        } while (choice!=0);
    }
    static void stocksCheck(){
        Scanner sv  = new Scanner(System.in);
        for (int i=0;i<stocks.size();i++){
            if(stocks.get(i).bookCount<=5){
                System.out.println("Stock Needed: "+stocks.get(i).bookName);
                System.out.println("Enter the copy to add:");
                int count =sv.nextInt();
                stocks.get(i).bookCount+=count;
            }
        }
    }

}


