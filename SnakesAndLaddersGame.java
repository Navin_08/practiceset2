package com.practice.set2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class SnakesAndLaddersGame  {
      int noOfPlayer=0;
    static HashMap<Integer,Integer> board = new HashMap<>();

//     void setLadderPosition(){
//        System.out.println("ladder position set:");
//        board.put(2,23);
//        board.put(8,34);
//        board.put(20,77);
//        board.put(32,68);
//        board.put(41,79);
//        board.put(74,88);
//        board.put(82,100);
//        board.put(85,95);
//    }
//     void setSnakePosition(){
//        System.out.println("Snake position Set:");
//        board.put(97,25);
//        board.put(92,70);
//        board.put(86,54);
//        board.put(62,37);
//        board.put(53,33);
//        board.put(47,5);
//        board.put(38,15);
//        board.put(29,9);
//    }
//    SnakesAndLaddersGame(){
//       this.setSnakePosition();
//       this.setLadderPosition();
//    }
    static {
        System.out.println("Snake position Set:");
        board.put(97,25);
        board.put(92,70);
        board.put(86,54);
        board.put(62,37);
        board.put(53,33);
        board.put(47,5);
        board.put(38,15);
        board.put(29,9);

        System.out.println("ladder position set:");
        board.put(2,23);
        board.put(8,34);
        board.put(20,77);
        board.put(32,68);
        board.put(41,79);
        board.put(74,88);
        board.put(82,100);
        board.put(85,95);
    }

    void setNoOfPlayer(){
        System.out.println("Enter the number of player:");
        Scanner sc  = new Scanner(System.in);
        noOfPlayer =sc.nextInt();   //FIXME: The noOfPlayer variable should not be static.
    }

    void startGame(){
        //Player players[] = new Player[5];
        ArrayList<Player> players = new ArrayList<>();
        Scanner nm = new Scanner(System.in);
        for(int i = 0; i< noOfPlayer; i++){
            System.out.printf("Enter the player %d name:",i+1);
            String name=nm.next();
            players.add(new Player(name));    //FIXME: REMOVE THE POSITION VARIABLE. By Default, the value of position is 0. No need to set it explicitly.
       }
        System.out.println("Game started");
        int pos=0;
        while (pos<=100){
            int roll;
            for(int i = 0; i< players.size(); i++){

                System.out.println(players.get(i).name+" Turn: ");

                String in = nm.nextLine();
                roll = rollDice();

                System.out.println(players.get(i).name+" Rolls Dice "+roll);

                if(players.get(i).position==0){
                    if(roll==1){
                        pos =roll;
                        players.get(i).position=pos;
                    }else{
                        continue;
                    }
                }else{
                    pos = players.get(i).position;
                    pos+=roll;
                    if(pos==100){
                        System.out.println(players.get(i).name + " Has Won");
                        return ;
                    }
                    else if(pos <=100){
                        int pre = pos;
                        if(SnakesAndLaddersGame.board.containsKey(pos)){
                           pos=SnakesAndLaddersGame.board.get(pos);
                           if(pos<pre){
                               System.out.println(players.get(i).name+ " Stepped on Snake at position: "+pre+" to "+pos);
                           }else if(pos>pre){
                               System.out.println(players.get(i).name+" Stepped on Ladder at position: "+pre+" to "+pos);
                           }
                        }
                        players.get(i).position=pos;
                    }
                    else{
                        pos -=roll;
                        players.get(i).position=pos;

                    }
                }
                if(pos==100){
                    System.out.println(players.get(i).name + " Has Won");
                    return ;
                }
            }
            for(int j= 0; j< players.size(); j++){
                System.out.println(players.get(j).name+ " is at Position: "+ players.get(j).position);
            }
        }
    }

    public int rollDice(){
        int dice;
        int max=6;
        Random random = new Random();
        dice=random.nextInt(max)+1;
        return dice;
    }


    public static void main(String[] args){

        SnakesAndLaddersGame sl = new SnakesAndLaddersGame();
        sl.setNoOfPlayer();
        sl.startGame();
    }
}