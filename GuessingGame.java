package com.practice.set2;

import java.util.Random;
import java.util.Scanner;

//public class Guessing {
//	public static void main(String[] args){
//		Scanner sc  = new Scanner(System.in);
//
//		Random ran = new Random();
//		System.out.println("Guess the number from range 1 to 100");
//
//		int n = ran.nextInt(100)+1;
//	//	System.out.println(n);
//	int count=0;
//	while(count<5){
//		System.out.println("Enter the Expression to guess:");
//        String  gu = sc.next();
//		count++;
//
//		System.out.println("1.Addition:");
//		System.out.println("2.subtraction");
//		System.out.println("3.Multiplication");
//		System.out.println("4.Division");
//
//		char op = sc.next().charAt(0);
//
//		if(op=='1'){
//			gu+=n;
//		}else if(op=='2'){
//			gu-=n;
//		}else if(op=='3'){
//			gu/=n;
//		}else if(op=='4'){
//			gu%=n;
//		}
//
//		if(gu==n){
//			System.out.println("You picked correct number");
//			break;
//		}else if(gu >n){
//			System.out.println("Your picked greater than the number");
//		}else {
//			System.out.println("Your picked lesser than the number");
//		}
//
//		if(count==5){
//			System.out.println("Better luck next time");
//			System.out.println("The  number is "+n);
//		}
//	}
//	sc.close();
//	}
//}

public class GuessingGame {
     static int evaluate(String expression, int n) {
        int op = -1, res = 0;
        boolean f = false;
        StringBuilder first = new StringBuilder();
        StringBuilder second = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) >= '0' && expression.charAt(i) <= '9' && op==-1)
                first.append(expression.charAt(i));


        if ((expression.charAt(i) >='a' && expression.charAt(i)<='z') || (expression.charAt(i) >='A' && expression.charAt(i)<='Z')) {
            f = true;
        }

        if (expression.charAt(i) == '+') {  //fixme: better way
            op = 1;
        } else if (expression.charAt(i) == '-') {
            op = 2;
        } else if (expression.charAt(i) == '*') {
            op = 3;
        } else if (expression.charAt(i) == '/') {
            op = 4;
        } else if (expression.charAt(i) == '<') {
            op = 5;
        } else if (expression.charAt(i) == '>') {
            op = 6;
        } else if (expression.charAt(i) == '=') {
            op = 7;
        }

        if (op != -1) {
            for (int j = i + 1; j < expression.length(); j++) {
                second.append(expression.charAt(j));
            }
        }

    }
//        System.out.println(first);
//        System.out.println(second);

        int fst,sec=0;
            fst= Integer.parseInt(first.toString());

        if(op ==-1){
            sec =Integer.parseInt(second.toString());
        }

        if (op == 1 ) {
            if(f)
                res = fst + n;
            else
                res = fst + sec;
        } else if (op == 2) {
            if(f)
                res = fst - n;
            else
                res = fst - sec;
        } else if (op == 3) {
            if(f)
                res = fst * n;
            else
                res = fst * sec;
        } else if (op == 4) {
                if (f)
                    res = fst / n;
                else
                    try{
                        res = fst / sec;
                    }catch (ArithmeticException e){
                        System.out.println(e);
            }
        } else if (op == 5) {
            if (fst < n || (fst<sec)) {
                res = 1;
            }else
                res = 0;

        } else if (op == 6) {
            if (fst > n || (fst>sec)) {
                res = 1;
            }else
                res = 0;
        } else if (op == 7) {
            if (fst == n || (fst==sec)) {
                res = n;
            } else {
                res = 0;
            }
        }
        System.out.println(res);
        return res;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Random ran = new Random();
        System.out.println("Guess the number from range 1 to 100");
        int n = ran.nextInt(100) + 1;
        //System.out.println(n);
        int count = 0;

        while (count < 5) {

            System.out.println("Enter the expression to guess like (10<x),(10+2): ");
            String expression = sc.next();
            count++;
            int res = evaluate(expression,n);
            //System.out.println(res);

            if (res < n) {
                System.out.println("You picked number lesser");
            } else if (res > n) {
                System.out.println("you picked number greater");
            } else {
                System.out.println("You picked correct number");
                System.out.println(n);
                return;
            }
            if (count == 5) {
                System.out.println("Correct number: "+n);
                break;
            }
        }

    }
}