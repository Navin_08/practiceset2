package com.practice.set2;

import java.util.HashSet;
import java.util.Scanner;

public class FirstOccurrence {
        public static void bruteForce(String s) {
            System.out.println("Bruteforce approach:");
            for (int i = 0; i < s.length(); i++) {
                boolean b = false;
                for (int j = i + 1; j < s.length(); j++) {
                    if (s.charAt(i) == s.charAt(j)) {
                        b = true;
                    }
                }
                if (b) {
                    System.out.println("First Occurrence string is: " + s.charAt(i));
                    break;
                }
            }
        }

        public static void hashing(String s){
            HashSet<Character> set = new HashSet<>();
//            for(int i=0;i<s.length();i++){
//                if(set.contains(s.charAt(i))){
//                    System.out.println("First Occurrence string is: " + s.charAt(i));
//                    break;
//                }
//                set.add(s.charAt(i));
//            }
            for (char c:s.toCharArray()) {
                if(set.contains(c)){
                    System.out.println("First Occurrence string is: " + c);
                    break;
                }
                set.add(c);
            }
        }

        public static void withoutSet(String str){
            int[] alp = new int[256];
            boolean f= false;
            int i;
            for (i = 0; i < str.length(); i++) {
                int count = 0;
                int index;
                index = (int) str.charAt(i);
                if (alp[index] == 1) {
                    f=true;
                    break;
                } else {
                    alp[index] = count + 1;
                }
            }
            if(f){
                System.out.println("First Occurrence string is: " + str.charAt(i));
            }else{
                System.out.println("Not found");
            }
        }

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the string:");
            String str = sc.next();

            hashing(str);
            //bruteforce(str);
        }

}

