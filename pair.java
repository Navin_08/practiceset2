package com.practice.set2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class pair{
	public  static void find(int[] arr, int tar){
		for(int i=0;i<arr.length;i++){
			for(int j=1;j<arr.length;j++){
				if((arr[i]+arr[j])==tar){
					System.out.printf("Pair found: [%d,%d]",arr[i],arr[j]);
					return;
				}
			}
		}
		System.out.println("Not found");
	}

	public static void findPair(int[] arr, int tar){
		//HashMap<Integer,Integer> map = new HashMap<>();
		HashSet<Integer> set = new HashSet<>();
//		for(int i=0;i<arr.length;i++){
//			if(map.containsKey(map.get(tar-arr[i]))){
////				System.out.println(i);
//				System.out.printf("Pair found: (%d,%d)",arr[i],(tar-arr[i]));
//				break;
//			}
//			map.put(arr[i],i);
//			//System.out.println(i);
//		}
		for (int i=0;i<arr.length;i++) {
			if(set.contains(tar-arr[i])){
				System.out.printf("Pair found: (%d,%d)",arr[i],(tar-arr[i]));
				return;
			}
			set.add(arr[i]);
		}
		//System.out.println(map);
		System.out.println("Not found");
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length od the array:");
		int n = sc.nextInt();
		int[] arr = new int[n];

		System.out.println("Enter the target:");
		int tar = sc.nextInt();

		for (int i = 0; i < n; i++) {
			System.out.println("Element [" + i + "]:");
			arr[i] = sc.nextInt();
		}
		findPair(arr,tar);
	}
}
