package com.app;

public class ProgrammingBooks extends Books {

    ProgrammingBooks() {
        super();
    }

    ProgrammingBooks(String bookName, String authorName, int bookCount, int price, int bookId) {
        super(bookName, authorName, bookCount, price, bookId);
    }

    void addToStocks() {
        BookStore.stocks.add(new ProgrammingBooks("C", "Dennis", 100, 300, 801));
        BookStore.stocks.add(new ProgrammingBooks("Java", "James", 80, 200, 901));
        BookStore.stocks.add(new ProgrammingBooks("Python", "guido", 75, 100, 701));
    }
}
