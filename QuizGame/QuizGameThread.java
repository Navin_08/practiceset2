package com.quizgame;

import java.util.Scanner;


public class QuizGameThread extends Thread{
    String question;
    String[] choices;
    String answer;
    Participant participant;
    public QuizGameThread(String question, String[] choices, String answer, Participant participant){
        this.question= question;
        this.choices=choices;
        this.answer=answer;
        this.participant=participant;
    }
    public static boolean isDone =false;

    public void run(){
        int opt;
        System.out.println(System.lineSeparator().repeat(100));

        System.out.println(question);
        for (String choice : choices) {
            System.out.println(choice);
        }

        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the options:");
        opt = sc.nextInt();

        if(choices[opt-1].equalsIgnoreCase(answer)){
            participant.answerCorrect++;
            System.out.println("Correct Answer");
        }else{
            participant.answerWrong++;
            System.out.println("Wrong Answer");
        }
        isDone = true;
        return;
    }
    public void call() {

        start();
        System.out.println();
        long startTime = System.currentTimeMillis();
        while(!isAlive());
        while(System.currentTimeMillis()-startTime < 10000 && isAlive()){
//            System.out.println(t.getState());
//            System.out.println(t.isAlive());
        }
//        System.out.println(((MyThread)t).isDone);

        interrupt();
       // System.out.println(isAlive());
       // System.out.println("time exists");
        if(!isDone && isAlive()){
            System.out.println("time exists");
        }
        return ;
    }

}


