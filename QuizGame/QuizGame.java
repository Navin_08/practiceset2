package com.quizgame;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class QuizGame {
    static int noOfQuestion;
    static int timeOut;
    static int participantId=123;
     static ArrayList<Questions> questions = new ArrayList<>();
     static ArrayList<Participant> participants = new ArrayList<>();

     static void startQuiz(Participant participant) throws InterruptedException {

        System.out.println("Quiz Started:");
        int i;
        for (i=0;i<questions.size();i++) {
//            System.out.println(q.question);
//            int ch = 1;
//            for (int j = 0; j < 4; j++) {
//                System.out.println(ch + " " + q.choices[j]);
//                ch++;
//            }
//            System.out.println("Enter your choice : ");
            QuizGameThread t= new QuizGameThread(questions.get(i).question,questions.get(i).choices,questions.get(i).answer,participant);

        // MyThread t= new MyThread("ques",new String[]{"a","b"},"answer",new Participant("p","p",12));
            t.call();

//            long start = System.currentTimeMillis();
//            while(!t.isAlive());
//            while(System.currentTimeMillis()-start<10000  && ((MyThread)t).isAlive()){
//            }
//            boolean flag = true;
//            int ans = -1;
//
//            do {
//                try {
//                    ans = sc.nextInt();
//                    flag = false;
//                } catch (InputMismatchException e) {
//                    System.out.println("Enter number only!!!");
//                }
//            } while (flag);
//            try {
//                if (questions.get(i).choices[ans - 1].equalsIgnoreCase(questions.get(i).answer)) {
//                    participant.answerCorrect++;
//                } else {
//                    participant.answerWrong++;
//                }
//            } catch (Exception e) {
//                System.out.println("Select option only shown below");
//                participant.answerWrong++;
//            }
        }
        System.out.println(participant.name+" Scored:");
        System.out.println("  Answered Correct :"+participant.answerCorrect);
        System.out.println("  Answered Wrong :"+participant.answerWrong);
        QuizGame nextGame= new QuizGame();
        nextGame.Quiz();
        return;
    }

    public static void login() throws InterruptedException {
        Scanner sc  =new Scanner(System.in);
        int id = 0;
        System.out.println("Enter your Participant Id:");
        try{
            id =sc.nextInt();
        }catch (InputMismatchException e){
            System.out.println("Enter number only ");
        }
        boolean b =false;
        for (Participant participant : participants) {

            if (participant.participantId == id) {
                b = true;
                startQuiz(participant);
            }
        }
        if(!b){
            System.out.println("login Failed");
            login();
        }

    }
    static void createAccount(){
        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter password: ");
//        char[] password = console.readPassword("","Enter password");
        System.out.println("Enter the username");
        String name=sc.next();
        int id =participantId++;
        System.out.println("You Participant Id: "+id);
        participants.add(new Participant(name,id));
    }

       void interviewerLogin(){
        String name,password;
        boolean b= false;
        Scanner sc = new Scanner(System.in);
        Interviewer interviewer = new Interviewer();
        System.out.println("interviewer username ");
        name =sc.next();
        System.out.println("interviewer password ");
        password =sc.next();
        if(name.equalsIgnoreCase(interviewer.name) && (password.equalsIgnoreCase(interviewer.pass))){
            b=true;
            // interviewer.setNoOfQuestionsAndTimer();
            //interviewer.setQuestionsAndAnswers();
            Interviewer.addQuestions();
        }
        if(!b){
            System.out.println("Enter the correct username or password");
            interviewerLogin();
        }
    }
    void result(){
        System.out.println("Participant Result:");
        System.out.println("Name\t\t\tAnsweredCorrect\t\tAnsweredWrong");
        for (Participant participant : participants) {
            System.out.printf("%s\t\t\t\t%d\t\t\t\t\t\t%d\n", participant.name, participant.answerCorrect, participant.answerWrong);
        }
    }
      void Quiz() throws InterruptedException {
        int choice = 0;
        Scanner sc = new Scanner(System.in);

        do{
            System.out.println("1.Create Account");
            System.out.println("2.login");
            System.out.println("3.Exit");

            try {
                choice = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter number only");
            }

            switch (choice) {
                case 1:
                    QuizGame.createAccount();
                    break;
                case 2:
                    QuizGame.login();
                case 3:
                    result();
                    System.exit(0);
                default:
                    System.out.println("Enter the valid choice");
            }

        }while (choice !=0);
    }

    public static void main(String[] args) throws InterruptedException {
        QuizGame game  =new QuizGame();
        game.interviewerLogin();
        game.Quiz();
    }

}
