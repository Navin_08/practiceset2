package com.quizgame;


public class Participant {
    String name;

    int participantId;
    public int answerCorrect;
    public int answerWrong;

    Participant(String name,int participantId){
        this.name=name;
        this.participantId =participantId;
    }
}
