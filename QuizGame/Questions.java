package com.quizgame;


public class Questions {
    String question;
    public String[] choices ;
    public String answer;


    Questions(String question, String[] choices, String answer){
        this.question = question;
        this.choices = choices;
        this.answer = answer;
    }

}
